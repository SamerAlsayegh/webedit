#** WebEdit+** #

*WebEdit+ is an easy and user-friendly way to build your custom websites (mobile compatible) on the go, through your phone.*


** *By phones. For phones.* **


## How to use? ##
WebEdit+ will have a wiki with a how-to use and build your first website when we reach the beta stage of the application. (refer to information below)

## How to contribute? ##
More into will be placed here... At the moment, we don't accept forks or commits from the public. We may allow cloning of our Repo soon, but at this time, we don't allow it.

## Where can I get the app? ##
There is no ETA yet, but we are aiming to have a public release by the start of January (within the first 2 weeks of January).

## Development Stages ##
**Alpha** *will have most coding done, and we will be hunting for bugs and also working on the UI for simplicity.*

**Beta** *will have limited functionality in the sense that you can build a basic website, but there won't be any hosting options other than FTP. As we advance we will add the 'like to have' onto the project.*

**Public** *all the functionality we hope to have working, will be working fully by then.*