package com.SamerAlsayegh.Liveweb.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.SamerAlsayegh.Liveweb.Enums.Profile;
import com.SamerAlsayegh.Liveweb.sharing.Google_Plus;
import com.facebook.model.GraphUser;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.Person.Gender;

import android.annotation.SuppressLint;

public class ProfileBuilder {
	
	
	@SuppressLint("SimpleDateFormat") public static Profile buildFromGooglePlus(Person person){
		Profile prof = new Profile((person.getName().getGivenName().contains(" ") ? person.getName().getGivenName().split(" ")[0] : person.getName().getGivenName()), (person.getName().getGivenName().contains(" ") ? person.getName().getGivenName().split(" ")[person.getName().getGivenName().split(" ").length - 1] : ""));
		Date date;
		try {
			date = new SimpleDateFormat("dd/MM/yyyy").parse(person.getBirthday());
			prof.setBirthdate(date.getTime()/1000);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String email = Plus.AccountApi.getAccountName(Google_Plus.mGoogleApiClient);
		prof.setEmail(email);
		prof.setGender((person.getGender() == Gender.MALE) ? com.SamerAlsayegh.Liveweb.Enums.Gender.Male : (person.getGender() == Gender.FEMALE) ? com.SamerAlsayegh.Liveweb.Enums.Gender.Female : (person.getGender() == Gender.OTHER) ? com.SamerAlsayegh.Liveweb.Enums.Gender.Other : com.SamerAlsayegh.Liveweb.Enums.Gender.Undefined);
		
		return prof;
	}
	
	
	@SuppressLint("SimpleDateFormat") public static Profile buildFromFacebook(GraphUser user){
		Profile prof = new Profile(user.getFirstName(), user.getLastName());
		Date date;
		try {
			date = new SimpleDateFormat("dd/MM/yyyy").parse(user.getBirthday());
			prof.setBirthdate(date.getTime()/1000);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String email = user.getProperty("email").toString();
		prof.setEmail(email);
		String gender = user.asMap().get("gender").toString();
		System.out.println("Gender is "  + gender + " and email " + email);
		//prof.setGender);
		
		return prof;
	}
}
