package com.SamerAlsayegh.Liveweb.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.SamerAlsayegh.Liveweb.Enums.ObjectType;
import com.SamerAlsayegh.Liveweb.Enums.ProjectInfo;
import com.SamerAlsayegh.Liveweb.Objects.WebObject;

import android.os.Environment;

public class XMLProjectParser {


	public static ProjectInfo readProject(ProjectInfo projectInfo) throws XmlPullParserException, IOException{
		File data = new File(Environment.getExternalStorageDirectory() + "/WebEdit/Projects/" + projectInfo.getProjectName() + "/", "data");
		System.out.println("Doc Name " + projectInfo.getProjectName());
		if (!data.exists()) {
			data.mkdirs();
		}

		File dataFile = new File(data, "project.xml");
		FileInputStream fIn = new FileInputStream(dataFile);
		BufferedReader myReader = new BufferedReader(
				new InputStreamReader(fIn));
		String aDataRow = "";
		String aBuffer = "";
		while ((aDataRow = myReader.readLine()) != null) {
			aBuffer += aDataRow + "\n";
		}
		System.out.println("Doc " + aBuffer);
		myReader.close();

		XmlPullParserFactory factory = XmlPullParserFactory.newInstance();

		factory.setNamespaceAware(true);
		XmlPullParser xpp = factory.newPullParser();
		xpp.setInput(new StringReader (aBuffer));
		int eventType = xpp.getEventType();



		StringBuilder openedTag = new StringBuilder();
		WebObject object = new WebObject(null);
		String pageName = null;


		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_DOCUMENT) {
				Message.debug("Started Document XML parsing");
			} else if(eventType == XmlPullParser.START_TAG) {
				Message.debug("Started XML Tag @ " + xpp.getName());
				openedTag.append(xpp.getName() + ";");


				for (Integer subCount = 0; subCount <= xpp.getAttributeCount() - 1; subCount++){
					String tagName = xpp.getAttributeName(subCount);
					String tagValue = xpp.getAttributeValue(subCount);


					if (lastTags(openedTag.toString(), 0).equalsIgnoreCase("BasicInfo")){
						if (tagName.equalsIgnoreCase("Project-ID"))
							projectInfo.setProjectID(Integer.parseInt(tagValue));
						else if (tagName.equalsIgnoreCase("Created"))
							projectInfo.setProjectCreation(Integer.parseInt(tagValue));
						else if (tagName.equalsIgnoreCase("Last_Modified"))
							projectInfo.setProjectModified(Integer.parseInt(tagValue));
						else if (tagName.equalsIgnoreCase("WebsiteName"))
							projectInfo.setWebsiteName(tagValue);
					}
					Message.debug("Tags " + lastTags(openedTag.toString(), 0));
					if (lastTags(openedTag.toString(), 2).equalsIgnoreCase("Pages")){
						pageName = lastTags(openedTag.toString(), 1);
						projectInfo.addPage(pageName);
						if (xpp.getName().contains("Object-")){
							for (Integer subCountChild = 0; subCountChild < xpp.getAttributeCount(); subCountChild++){
								String tagNameChild = xpp.getAttributeName(subCountChild);
								String tagValueChild = xpp.getAttributeValue(subCountChild);
								if (tagNameChild.equalsIgnoreCase("Object-ID"))
									object.setID(tagValueChild);
								else if (tagNameChild.equalsIgnoreCase("ObjectType"))
									object.setObjectType(ObjectType.valueOf(tagValueChild));
								else if (tagNameChild.equalsIgnoreCase("ClassName"))
									object.setClassName(tagValueChild);

							}
						}
					}
					if (xpp.getName().equalsIgnoreCase("DataSet")){
						String tagNameChild = xpp.getAttributeName(subCount);
						String tagValueChild = xpp.getAttributeValue(subCount);
						object.addDataSet(tagNameChild, tagValueChild);
					}
					Message.debug("XML Tag " + xpp.getAttributeName(subCount) + " is " + xpp.getAttributeValue(subCount));
				}


			} else if(eventType == XmlPullParser.END_TAG){
				Message.debug("Ended XML Tag @ " + xpp.getName());
				if (xpp.getName().contains("Object-")){
					projectInfo.addClassesObject(pageName, object);
					object = new WebObject(null);
				}


				openedTag = new StringBuilder(openedTag.toString().replace(xpp.getName() + ";", ""));
			}
			else if(eventType == XmlPullParser.TEXT)
				Message.debug("Text XML Tag @ " + xpp.getName());
			eventType = xpp.next();
		}
		if(eventType == XmlPullParser.END_DOCUMENT)
			Message.debug("Ended Document XML parsing");
		return projectInfo;
	}

	private static String lastTag(String openedTags) {
		return openedTags.split(";")[openedTags.split(";").length - 1];
	}
	private static String lastTags(String openedTags, Integer count) {
		if (openedTags.split(";").length <= count)
			return openedTags;
		else
			return openedTags.split(";")[openedTags.split(";").length - 1 - count];
	}
}
