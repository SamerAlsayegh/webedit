package com.SamerAlsayegh.Liveweb.utilities;

import android.annotation.SuppressLint;

public class Message {
	static boolean enabledDebug = true;

	@SuppressLint("DefaultLocale") 
	public static String format(String fullstr, String replace, String find) {
		if (fullstr.contains(find))
			replace = fullstr.replace(find, replace);
		else
			replace = fullstr;		
		return replace;
	}


	public static String CleanCapitalize(String msg) {
		if (msg.contains("_"))
			msg = msg.replace('_', ' ');

		if (msg.contains("["))
			msg = msg.replace('[', ' ');

		if (msg.contains("]"))
			msg = msg.replace(']', ' ');


		msg = msg.toLowerCase().substring(0, 1).toUpperCase() + msg.toLowerCase().substring(1);

		return msg;
	}


	public static void debug(String string) {
		if (enabledDebug)
			System.out.println("[WebEdit+/DEBUG] " + string);
	}
}
