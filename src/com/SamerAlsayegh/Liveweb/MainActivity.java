/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.SamerAlsayegh.Liveweb;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xmlpull.v1.XmlPullParserException;

import com.SamerAlsayegh.Liveweb.Enums.FieldValueType;
import com.SamerAlsayegh.Liveweb.Enums.ObjectType;
import com.SamerAlsayegh.Liveweb.Enums.Profile;
import com.SamerAlsayegh.Liveweb.Enums.ProjectInfo;
import com.SamerAlsayegh.Liveweb.Fragments.homeFragment;
import com.SamerAlsayegh.Liveweb.Fragments.settingsFragment;
import com.SamerAlsayegh.Liveweb.Fragments.shareFragment;
import com.SamerAlsayegh.Liveweb.Fragments.unknownFragment;
import com.SamerAlsayegh.Liveweb.Objects.WebObject;
import com.SamerAlsayegh.Liveweb.customObjects.Button_F1;
import com.SamerAlsayegh.Liveweb.sharing.Facebook;
import com.SamerAlsayegh.Liveweb.sharing.Google_Plus;
import com.SamerAlsayegh.Liveweb.utilities.Message;
import com.SamerAlsayegh.Liveweb.utilities.XMLProjectParser;
import com.facebook.AppEventsLogger;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.plus.Plus;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

@SuppressWarnings("deprecation")
public class MainActivity extends FragmentActivity implements ActionBar.TabListener, OnClickListener, ConnectionCallbacks, OnConnectionFailedListener, com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener {
	private static final String TAG = "WebEdit+";
	static ArrayList<String> menuOptions = new ArrayList<String>();
	public static Profile loggedInUser = null;
	public static View homeFragmentView = null;
	ListView listView = null;

	/*
	 * Hashmap that will handle the field requests...
	 * Will get them the values needed for the addition of an object, such as Text, Image, Video, Link
	 * Based on the item wanted to be added it will be put in... and ask for the fields...
	 */
	public static LinkedHashMap<String, ProjectInfo> projects = new LinkedHashMap<String, ProjectInfo>();
	public static String openedProject;


	static LinkedHashMap<ObjectType, LinkedHashMap<String, FieldValueType>> fieldPlaceHolders = new LinkedHashMap<ObjectType, LinkedHashMap<String, FieldValueType>>();
	// List to contain 
	public static MainActivity instance;
	public static UiLifecycleHelper uiHelper;
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the
	 * three primary sections of the app. We use a {@link android.support.v4.app.FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	AppSectionsPagerAdapter mAppSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will display the three primary sections of the app, one at a
	 * time.
	 */
	ViewPager mViewPager;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
		setContentView(R.layout.activity_main);

		File f = new File(Environment.getExternalStorageDirectory() + "/WebEdit/Projects/");
		File[] files = f.listFiles();
		f.mkdirs();
		for (File inFile : files) {
			File inFolder = new File(Environment.getExternalStorageDirectory() + "/WebEdit/Projects/" + inFile.getName() + "/", "data");
			File dataFile = new File(inFolder, "project.xml");
			if (inFile.isDirectory()) {
				if (dataFile.isFile()){
					Message.debug("Project Added: " + inFile.getName());
					MainActivity.projects.put(inFile.getName(), new ProjectInfo(inFile.getName()));
				}
			}
		}


		SharedPreferences appConf = this.getSharedPreferences("Information", Context.MODE_PRIVATE);
		openedProject = appConf.getString("LastProject", null);
		File data = new File(Environment.getExternalStorageDirectory() + "/WebEdit/Projects/" + openedProject + "/", "data");
		if (data.exists()) {
			File dataFile = new File(data, "project.xml");
			if (!dataFile.exists())
				openedProject = null;
		}
		else
			openedProject = null;


		System.out.println("Last Project: " + openedProject);
		if (openedProject != null){
			try {
				MainActivity.projects.put(openedProject, XMLProjectParser.readProject(MainActivity.projects.get(openedProject)));
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/*
		 * Set-up main menu - Top bar options
		 */
		menuOptions.clear();
		menuOptions.add("Share");
		menuOptions.add("Home");
		menuOptions.add("Settings");


		// Create the adapter that will return a fragment for each of the three primary sections
		// of the app.
		mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());
		// Set up the ViewPager, attaching the adapter and setting up a listener for when the
		// user swipes between sections.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mAppSectionsPagerAdapter);
		mViewPager.setCurrentItem(1);
		/*
		 * Field Fillers (Fields to need to add object...)
		 * Field which have an asterisk (*) are required fields.
		 */
		fieldPlaceHolders.clear();
		ObjectType obType = ObjectType.Text;
		fieldPlaceHolders.put(obType, new LinkedHashMap<String, FieldValueType>());
		fieldPlaceHolders.get(obType).put("*Text to Display", FieldValueType.HTML_String);
		fieldPlaceHolders.get(obType).put("*ID", FieldValueType.ID);
		fieldPlaceHolders.get(obType).put("Customizations", FieldValueType.StyleBuilder);
		fieldPlaceHolders.get(obType).put("Pre-configured Styles", FieldValueType.Dropdown);
		fieldPlaceHolders.get(obType).put("Name", FieldValueType.String);
		
		obType = ObjectType.Image;
		fieldPlaceHolders.put(obType, new LinkedHashMap<String, FieldValueType>());
		fieldPlaceHolders.get(obType).put("*Image", FieldValueType.Image);
		fieldPlaceHolders.get(obType).put("*ID", FieldValueType.ID);
		fieldPlaceHolders.get(obType).put("Customizations", FieldValueType.StyleBuilder);
		fieldPlaceHolders.get(obType).put("Pre-configured Styles", FieldValueType.Dropdown);
		fieldPlaceHolders.get(obType).put("Description", FieldValueType.String);
		fieldPlaceHolders.get(obType).put("Name", FieldValueType.String);
		
		obType = ObjectType.Link;
		fieldPlaceHolders.put(obType, new LinkedHashMap<String, FieldValueType>());
		fieldPlaceHolders.get(obType).put("*Text to Display", FieldValueType.HTML_Link);
		fieldPlaceHolders.get(obType).put("*ID", FieldValueType.ID);
		fieldPlaceHolders.get(obType).put("*Link", FieldValueType.StyleBuilder);
		fieldPlaceHolders.get(obType).put("Pre-configured Styles", FieldValueType.Dropdown);
		fieldPlaceHolders.get(obType).put("Name", FieldValueType.String);
		
		obType = ObjectType.Page;
		fieldPlaceHolders.put(obType, new LinkedHashMap<String, FieldValueType>());
		fieldPlaceHolders.get(obType).put("*Page Name", FieldValueType.String);
		
		/*
		 * Initialize the fonts we will be using
		 */

		/*
		 * Initialize Facebook
		 */
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);
		/*
		 * Initialize Google
		 */
		Google_Plus.mGoogleApiClient = new GoogleApiClient.Builder(this)
		.addConnectionCallbacks(this)
		.addOnConnectionFailedListener(this)
		.addApi(Plus.API)
		.addScope(Plus.SCOPE_PLUS_LOGIN)
		.build();
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
	 * sections of the app.
	 */
	public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {

		public AppSectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int i) {
			switch (i) {
			case 0:
				return new shareFragment();
			case 1:
				return new homeFragment();
			case 2:
				return new settingsFragment();
			default:
				return new unknownFragment();
			}
		}

		@Override
		public int getCount() {
			return MainActivity.menuOptions.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return MainActivity.menuOptions.get(position);
		}
	}

	public void checkResultOfFTP(View view){
		LinearLayout linearLayout = (LinearLayout) MainActivity.instance.findViewById(R.id.FTPPane);
		RadioGroup rG = (RadioGroup) MainActivity.instance.findViewById(R.id.ftpChoice);
		if (rG.getCheckedRadioButtonId() != R.id.EnableFTP){
			linearLayout.setVisibility(View.GONE);
			// Create Dialog for password...
			// custom dialog
			final Dialog dialog = new Dialog(instance, R.style.NewDialog);
			dialog.setContentView(R.layout.dialog_siginup);
			dialog.setCanceledOnTouchOutside(true);

			dialog.show();
		}
		else if (rG.getCheckedRadioButtonId() == R.id.EnableFTP)
			linearLayout.setVisibility(View.VISIBLE);
	}

	@Override
	public void onResume() {
		super.onResume();
		AppEventsLogger.activateApp(this);
		Session session = Session.getActiveSession();
		if (session != null &&
				(session.isOpened() || session.isClosed()) ) {
			onSessionStateChange(session, session.getState(), null);
		}
		uiHelper.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		AppEventsLogger.deactivateApp(this);
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	public static Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private static void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if (state.isOpened()) {
			Log.i(TAG, "Logged in...");
			if (loggedInUser == null)
				Facebook.getUserData(session, session.getState());
		} else if (state.isClosed()) {
			Log.i(TAG, "Logged out...");
		}
	}

	public void facebookSigninDialog (View view){
		final Dialog dialog = new Dialog(instance, R.style.NewDialog);
		dialog.setContentView(R.layout.dialog_facebook_signin);
		dialog.setCanceledOnTouchOutside(true);
		dialog.findViewById(R.id.authButton).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (Session.getActiveSession().isOpened()){
					Session.getActiveSession().closeAndClearTokenInformation();
					Facebook.graphUser = null;
					MainActivity.loggedInUser = null;
					Facebook.loggedIn = false;
				}
				else{
					Session session = Session.getActiveSession();
					if (session == null) {
						session = new Session(MainActivity.instance);
					}
					if (Google_Plus.loggedIn){
						Google_Plus.loggedIn = false;
						Google_Plus.signOutFromGplus();
					}
					Facebook.loggedIn = true;
					Session.setActiveSession(session);
					Session.OpenRequest request = new Session.OpenRequest(instance).setCallback(MainActivity.callback);
					request.setPermissions(Arrays.asList("publish_actions", "user_likes", "email", "user_friends"));
					if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
						session.openForPublish(request);
					} else {
						session = Session.openActiveSession(MainActivity.instance, true, MainActivity.callback);
					}
					Facebook.getUserData(session, session.getState());
				}
				dialog.dismiss();
			}
		});
		dialog.show();
	}


	public void googleSigninDialog (View view){
		final Dialog dialog = new Dialog(instance, R.style.NewDialog);
		dialog.setContentView(R.layout.dialog_google_signin);
		Google_Plus.btnSignIn = (SignInButton) dialog.findViewById(R.id.btn_sign_in);
		Google_Plus.btnSignOut = (Button) dialog.findViewById(R.id.btn_sign_out);

		// Button click listeners
		Google_Plus.btnSignIn.setOnClickListener(this);
		Google_Plus.btnSignOut.setOnClickListener(this);

		if (Google_Plus.loggedIn){
			Google_Plus.btnSignIn.setVisibility(View.GONE);
			Google_Plus.btnSignOut.setVisibility(View.VISIBLE);
		}

		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

	public void signupDialog (View view){
		final Dialog dialog = new Dialog(instance, R.style.NewDialog);
		dialog.setContentView(R.layout.dialog_siginup);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}


	public static void refreshGlobalDependance() {
		homeFragment.setupFBLogin();
	}


	public void newProject (View view){
		final Dialog dialog = new Dialog(instance, R.style.NewDialog);
		dialog.setContentView(R.layout.dialog_create_new_project);
		dialog.setCanceledOnTouchOutside(true);
		TextView title = (TextView) dialog.findViewById(R.id.mainTitle);
		title.setText(instance.getString(R.string.create_new_project));

		dialog.findViewById(R.id.submitCreation).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// Create project!
				// We will now open Editor at index.html page.
				EditText textWebsite = (EditText) dialog.findViewById(R.id.main_headline);
				EditText text = (EditText) dialog.findViewById(R.id.projectName_input);
				try {
					if (!text.getText().toString().isEmpty()){
						if (addProject(text.getText().toString(), textWebsite.getText().toString())){
							dialog.dismiss();
						}
					}
					else{
						Toast.makeText(instance, "Please enter a project name...", Toast.LENGTH_SHORT).show();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		dialog.findViewById(R.id.cancelCreation).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

	protected boolean addProject(String projectName, String websiteName) throws IOException {
		MainActivity.openedProject = projectName;
		ProjectInfo project = new ProjectInfo(openedProject);

		project.addPage("index");
		EditorActivity.loadedPage = "index";

		project.setWebsiteName(websiteName);
		projects.put(openedProject, project);

		updateXML(projectName);
		rebuildAllFiles(projectName);

		Intent intent = new Intent(instance, EditorActivity.class);
		startActivity(intent);
		return true;
	}

	public static void updateXML(String openedProject) {
		File data = new File(Environment.getExternalStorageDirectory() + "/WebEdit/Projects/" + openedProject + "/", "data");
		if (!data.exists()) {
			data.mkdirs();
		}

		try {
			// root elements
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
			Element rootElement = doc.createElement("ProjectInfo");
			doc.appendChild(rootElement);

			// Basic Project details... 
			Element basicInfo = doc.createElement("BasicInfo");
			rootElement.appendChild(basicInfo);

			basicInfo.setAttribute("Project-ID", "1");
			basicInfo.setAttribute("Created", ""+System.currentTimeMillis()/1000);
			basicInfo.setAttribute("Last_Modified", ""+System.currentTimeMillis()/1000);
			basicInfo.setAttribute("WebsiteName", MainActivity.projects.get(openedProject).getWebsiteName());

			if (projects.get(openedProject).getPages().size() >= 1){
				Element pages = doc.createElement("Pages");
				rootElement.appendChild(pages);
				for (Entry<String, LinkedList<WebObject>> pageInfo : projects.get(openedProject).getObjects()){

					Message.debug("Doc " + pageInfo.getKey());
					Element page = doc.createElement(pageInfo.getKey());
					pages.appendChild(page);
					int num = 0;

					for (WebObject object : pageInfo.getValue()){
						num++;
						Message.debug("Object " + object.getID());
						Element objectInfo = doc.createElement("Object-" + num);
						page.appendChild(objectInfo);

						objectInfo.setAttribute("Object-ID", object.getID());
						if (object.getClassName() != null)
							objectInfo.setAttribute("Class_Name", object.getClassName());

						objectInfo.setAttribute("ObjectType", object.getObjectType().name());
						Element dataSet = doc.createElement("DataSet");
						objectInfo.appendChild(dataSet);
						for (Entry<String, String> dataset : object.getFullDataSet().entrySet()){
							dataSet.setAttribute(dataset.getKey(), dataset.getValue());
						}
					}
				}
			}
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(data, "project.xml"));

			transformer.transform(source, result);

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}

	static void rebuildAllFiles(String openedProject) {
		// TODO Auto-generated method stub
		SharedPreferences lastProject = instance.getSharedPreferences("Information" , Context.MODE_PRIVATE);
		SharedPreferences.Editor lastProjectEditor = lastProject.edit();
		lastProjectEditor.putString("LastProject", projects.get(openedProject).getProjectName());
		lastProjectEditor.commit();

		Button btn;

		if (projects.size() > 0){
			btn = (Button) homeFragmentView.findViewById(R.id.continueLastProject);
			if (MainActivity.openedProject != null)
				btn.setVisibility(View.VISIBLE);
			btn = (Button) homeFragmentView.findViewById(R.id.newProject);

			btn = (Button) homeFragmentView.findViewById(R.id.loadProject);
			if (MainActivity.projects.size() > 1)
				btn.setVisibility(View.VISIBLE);
		}
		/*
		 * Font set-up
		 */


		File code = new File(Environment.getExternalStorageDirectory() + "/WebEdit/Projects/" + MainActivity.projects.get(MainActivity.openedProject).getProjectName() + "/", "code");
		if (!code.exists()) {
			code.mkdirs();
		}

		for (Entry<String, LinkedList<WebObject>> pageEntries : MainActivity.projects.get(openedProject).getObjects()){
			File gpxfile = new File(code, pageEntries.getKey() + ".html");
			FileWriter writer;
			try {
				writer = new FileWriter(gpxfile);
				writer.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">" +
						"<html>" +
						"<head>" +
						"<meta http-equiv=\"Content-Type\" content=\"application/xhtml+xml; charset=utf-8\" />" +
						"<!-- Meta tags that set up the page as a mobile page   -->" +
						"<meta name = \"viewport\" content = \"user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width\"><meta name=\"apple-mobile-web-app-capable\" content=\"yes\" />" +
						"<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\" />" +
						"<link rel=\"apple-touch-icon\" href=\"/m/images/iphone.png\" />" +
						"<meta name=\"format-detection\" content=\"telephone=no\" />" +
						"<!--  A free Google web font embed because android does not have the browser safe fonts -->" +
						"<link rel=\"stylesheet\" type=\"text/css\" href=\"http://fonts.googleapis.com/css?family=Allerta\">" +
						"<!-- Style sheet for NOT smart devices like older web phones  -->" +
						"<link href=\"/m/css/handheld.css\" rel=\"stylesheet\" type=\"text/css\" media=\"handheld\" />" +
						"<!-- Style sheet for smartphones and tablet devices -->" +
						"<link rel=\"stylesheet\" href=\"/m/css/ios.css\" media=\"screen\" />" +
						"<!-- Style sheet for lousy incompatible frustrating IE mobile -->" +
						"<!--[if IEMobile]><link rel=\"stylesheet\" href=\"/m/css/iemobile.css\" media=\"screen\" /><![endif]--><!-Script that scrolls page up to the top -->" +
						"<script type=\"text/javascript\">" +
						"window.scrollTo(0,1);" +
						"</script>" +
						"<!-- Fill out the Title for each page -->" +
						"<title>Samer Alsayegh - WebEdit+</title>" +
						"</head>");
				for (WebObject object : pageEntries.getValue()){
					if (object.getObjectType().equals(ObjectType.Text)){
						writer.append("<p" + (object.getClassObject() != null ? " class = \"" + object.getClassName() + "\"" : "") + ">" + object.getDataSet("Text") + "</p>");
					}
				}
				writer.append("<hr>" +
						"<footer style='text-align: center; width: auto; height: auto; background: #EBEBEB;'>Powered by <a href='http://www.SamerAlsayegh.com' >WebEdit+</a></footer>");
				writer.flush();
				writer.close();
				if (pageEntries.getValue().size() >= 1){
					Message.debug("Reloading...");
					//EditorActivity.reloadCurrentPage(openedProject);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	protected void onStart() {
		super.onStart();
		Google_Plus.mGoogleApiClient.connect();
	}

	protected void onStop() {
		super.onStop();

		if (Google_Plus.mGoogleApiClient.isConnected()) {
			Google_Plus.mGoogleApiClient.disconnect();
		}
	}


	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (!result.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
					0).show();
			return;
		}

		if (!Google_Plus.mIntentInProgress) {
			// Store the ConnectionResult for later usage
			Google_Plus.mConnectionResult = result;

			if (Google_Plus.mSignInClicked) {
				// The user has already clicked 'sign-in' so we attempt to
				// resolve all
				// errors until the user is signed in, or they cancel.
				Google_Plus.resolveSignInError();
			}
		}

	}


	@Override
	public void onConnected(Bundle arg0) {
		Google_Plus.connectedEvent();
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		Google_Plus.mGoogleApiClient.connect();
		Google_Plus.updateUI(false);
	}

	/**
	 * Button on click listener
	 * */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_sign_in:
			// Signin button clicked
			Google_Plus.signInWithGplus();
			if (Facebook.loggedIn){
				Facebook.loggedIn = false;
				Session.getActiveSession().closeAndClearTokenInformation();
				Facebook.graphUser = null;
				MainActivity.loggedInUser = null;
				MainActivity.refreshGlobalDependance();
			}
			break;
		case R.id.btn_sign_out:
			// Signout button clicked
			Google_Plus.signOutFromGplus();
			break;
		}
	}


	/*
	 * Activity Result - When we attempt a login...
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Google_Plus.RC_SIGN_IN) {
			if (resultCode != RESULT_OK) {
				Google_Plus.mSignInClicked = false;
			}

			Google_Plus.mIntentInProgress = false;

			if (!Google_Plus.mGoogleApiClient.isConnecting()) {
				Google_Plus.mGoogleApiClient.connect();
			}
		}
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	public void continueProject(View view){
		if (MainActivity.openedProject != null){
			EditorActivity.loadedPage = "index";
			Intent intent = new Intent(instance, EditorActivity.class);
			startActivity(intent);
		}
	}

	public void loadProject(View view){
		final Dialog dialog = new Dialog(instance, R.style.NewDialog);
		dialog.setContentView(R.layout.dialog_load_project);
		dialog.setCanceledOnTouchOutside(true);


		// Get ListView object from xml
		listView = (ListView) dialog.findViewById(R.id.projectsList);

		// Defined Array values to show in ListView
		String[] values = projects.keySet().toArray(new String[projects.keySet().size()]);

		// Define a new Adapter
		// First parameter - Context
		// Second parameter - Layout for the row
		// Third parameter - ID of the TextView to which the data is written
		// Forth - the Array of data

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.load_menu_list, R.id.Itemname, values);


		// Assign adapter to ListView
		listView.setAdapter(adapter); 

		// ListView Item Click Listener
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// ListView Clicked item value
				String itemValue = (String) listView.getItemAtPosition(position);
				MainActivity.openedProject = itemValue;
				continueProject(view);
				dialog.cancel();
			}
		}); 


		listView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				// ListView Clicked item value
				final String itemValue = (String) listView.getItemAtPosition(position);
				final Dialog dialog = new Dialog(instance, R.style.NewDialog);
				dialog.setContentView(R.layout.dialog_create_new_project);
				dialog.setCanceledOnTouchOutside(true);
				dialog.findViewById(R.id.projectName_input).setVisibility(View.GONE);
				dialog.findViewById(R.id.main_headline).setVisibility(View.GONE);
				Button_F1 cancel = (Button_F1) dialog.findViewById(R.id.cancelCreation);
				Button_F1 submit = (Button_F1) dialog.findViewById(R.id.submitCreation);
				cancel.setText("Cancel");
				submit.setText("Delete");
				submit.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						deleteProject(itemValue);
						dialog.cancel();
					}
				});
				cancel.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View v) {
						dialog.cancel();
					}
				});
				dialog.show();
				return true;
			}
		}); 


		dialog.findViewById(R.id.cancelLoadProject).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// Cancel the load menu.
				dialog.cancel();
			}
		});

		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}


	private void deleteProject(String projectName) {
		deleteDirectory(new File(Environment.getExternalStorageDirectory() + "/WebEdit/Projects/" + projectName + "/"));
		projects.remove(projectName);

		// Defined Array values to show in ListView
		String[] values = projects.keySet().toArray(new String[projects.keySet().size()]);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.load_menu_list, R.id.Itemname, values);

		// Assign adapter to ListView
		listView.setAdapter(adapter); 
	}
	
	public boolean deleteDirectory(File path) {
		if( path.exists() ) {
			File[] files = path.listFiles();
			if (files == null) {
				return true;
			}
			for(int i=0; i<files.length; i++) {
				if(files[i].isDirectory()) {
					deleteDirectory(files[i]);
				}
				else {
					files[i].delete();
				}
			}
		}
		return( path.delete() );
	}
}
