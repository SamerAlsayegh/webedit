package com.SamerAlsayegh.Liveweb.sharing;

import android.content.IntentSender.SendIntentException;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.SamerAlsayegh.Liveweb.MainActivity;
import com.SamerAlsayegh.Liveweb.utilities.ProfileBuilder;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

public class Google_Plus {

	public static SignInButton btnSignIn;
	public static Button btnSignOut;
	public static boolean loggedIn = false;
	/**
	 * A flag indicating that a PendingIntent is in progress and prevents us
	 * from starting further intents.
	 */
	public static boolean mIntentInProgress;

	public static boolean mSignInClicked;

	public static ConnectionResult mConnectionResult;

	public static final int RC_SIGN_IN = 0;

	// Google client to interact with Google API
	public static GoogleApiClient mGoogleApiClient;

	/**
	 * Sign-in into google
	 * */
	public static void signInWithGplus() {
		if (!mGoogleApiClient.isConnecting()) {
			mSignInClicked = true;
			loggedIn = true;
			if (!mGoogleApiClient.isConnected())
				resolveSignInError();
		}
	}


	/**
	 * Updating the UI, showing/hiding buttons and profile layout
	 * */
	public static void updateUI(boolean isSignedIn) {
		if (btnSignIn != null || btnSignOut != null){
			if (isSignedIn) {
				btnSignIn.setVisibility(View.GONE);
				btnSignOut.setVisibility(View.VISIBLE);
			} else {
				btnSignIn.setVisibility(View.VISIBLE);
				btnSignOut.setVisibility(View.GONE);
			}
		}
	}


	/**
	 * Fetching user's information name, email, profile pic
	 * */
	private static void getProfileInformation() {
		try {
			if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
				Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
				MainActivity.loggedInUser = ProfileBuilder.buildFromGooglePlus(currentPerson);
			} else {
				Toast.makeText(MainActivity.instance.getApplicationContext(), "Person information is null", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sign-out from google
	 * */
	public static void signOutFromGplus() {
		if (mGoogleApiClient.isConnected()) {
			Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			mGoogleApiClient.disconnect();
			mGoogleApiClient.connect();
			MainActivity.loggedInUser = null;
			loggedIn = false;
			updateUI(false);
		}
	}

	/**
	 * Method to resolve any signin errors
	 * */
	public static void resolveSignInError() {
		if (mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				mConnectionResult.startResolutionForResult(MainActivity.instance, RC_SIGN_IN);
			} catch (SendIntentException e) {
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}


	public static void connectedEvent() {
		mSignInClicked = false;
		if (MainActivity.loggedInUser == null){
			loggedIn = true;
			// Get user's information
			getProfileInformation();
			// Update the UI after signin
			updateUI(true);
		}
	}
}
