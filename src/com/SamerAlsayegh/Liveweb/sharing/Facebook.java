package com.SamerAlsayegh.Liveweb.sharing;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.SamerAlsayegh.Liveweb.MainActivity;
import com.SamerAlsayegh.Liveweb.R;
import com.SamerAlsayegh.Liveweb.utilities.Message;
import com.SamerAlsayegh.Liveweb.utilities.ProfileBuilder;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;

public class Facebook {

	public static Button shareButton;
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions", "user_likes", "email", "user_friends");
	public static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	public static boolean pendingPublishReauthorization = false;
	public static GraphUser graphUser;
	public static boolean loggedIn = false;


	public static void publishStory() {
		Session session = Session.getActiveSession();

		if (session != null){

			// Check for publish permissions    
			List<String> permissions = session.getPermissions();
			if (!isSubsetOf(PERMISSIONS, permissions)) {
				pendingPublishReauthorization = true;
				Session.NewPermissionsRequest newPermissionsRequest = new Session
						.NewPermissionsRequest(MainActivity.instance, PERMISSIONS);
				session.requestNewPublishPermissions(newPermissionsRequest);
				return;
			}

			Bundle postParams = new Bundle();
			postParams.putString("name", "WebEdit+ for Android");
			postParams.putString("caption", "Build amazing websites on the go!");
			postParams.putString("description", "A sleek and innovative app to help you build a website on the go.");
			postParams.putString("link", "https://SamerAlsayegh.com");
			postParams.putString("picture", "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");

			Request.Callback callback= new Request.Callback() {
				public void onCompleted(Response response) {
					JSONObject graphResponse = response
							.getGraphObject()
							.getInnerJSONObject();
					String postId = null;
					try {
						postId = graphResponse.getString("id");
					} catch (JSONException e) {
						Log.i("WebEdit+",
								"JSON error "+ e.getMessage());
					}
					FacebookRequestError error = response.getError();
					if (error != null) {
						Toast.makeText(MainActivity.instance
								.getApplicationContext(),
								error.getErrorMessage(),
								Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(MainActivity.instance
								.getApplicationContext(), 
								postId,
								Toast.LENGTH_LONG).show();
					}
				}
			};

			Request request = new Request(session, "me/feed", postParams, 
					HttpMethod.POST, callback);

			RequestAsyncTask task = new RequestAsyncTask(request);
			task.execute();
		}

	}

	private static boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}



	public static void getUserData(Session session, SessionState state)
	{
		if (state.isOpened())
		{
			Request.newMeRequest(session, new Request.GraphUserCallback()
			{
				@Override
				public void onCompleted(GraphUser user, Response response)
				{
					if (response != null)
					{
						try
						{
							graphUser = user;
							MainActivity.loggedInUser = ProfileBuilder.buildFromFacebook(user);
						}
						catch (Exception e)
						{
							e.printStackTrace();
							System.out.println("Exception e");
						}

					}
				}
			}).executeAsync();
		}
	}

}
