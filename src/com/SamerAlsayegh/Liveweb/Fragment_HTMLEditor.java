package com.SamerAlsayegh.Liveweb;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ImageButton;

import com.SamerAlsayegh.Liveweb.Fragments.unknownFragment;
import com.SamerAlsayegh.Liveweb.Fragments_HTML.advancedFragment;
import com.SamerAlsayegh.Liveweb.Fragments_HTML.basicFragment;


@SuppressWarnings("deprecation")
public class Fragment_HTMLEditor extends Fragment implements ActionBar.TabListener{
	public static Fragment_HTMLEditor instance;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment

		instance = this;
		View view = inflater.inflate(R.layout.activity_htmleditor_fragment, container, false);
		mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(instance.getChildFragmentManager());
		// Set up the ViewPager, attaching the adapter and setting up a listener for when the
		// user swipes between sections.
		mViewPager = (ViewPager) view.findViewById(R.id.htmlPager);
		mViewPager.setAdapter(mAppSectionsPagerAdapter);
		mViewPager.setCurrentItem(0);
		menuOptions.add("Basic");
		menuOptions.add("Advanced");
		mViewPager.setAdapter(new AppSectionsPagerAdapter(instance.getChildFragmentManager()));
		return view;
	}
	
	
	AppSectionsPagerAdapter mAppSectionsPagerAdapter;
	public static ViewPager mViewPager;

	static ArrayList<String> menuOptions = new ArrayList<String>();


	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
	 * sections of the app.
	 */
	public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {

		public AppSectionsPagerAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);
		}

		@Override
		public Fragment getItem(int i) {
			switch (i) {
			case 0:
				return new basicFragment();
			case 1:
				return new advancedFragment();
			default:
				return new unknownFragment();
			}
		}

		@Override
		public int getCount() {
			return menuOptions.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return menuOptions.get(position);
		}
	}

}
