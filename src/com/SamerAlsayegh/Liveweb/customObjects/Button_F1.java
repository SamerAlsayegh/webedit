package com.SamerAlsayegh.Liveweb.customObjects;

import com.SamerAlsayegh.Liveweb.EditorActivity;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

public class Button_F1 extends Button {

    public Button_F1(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Button_F1(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Button_F1(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/jaapokki-regular.ttf");
        setTypeface(tf);
    }
}
