package com.SamerAlsayegh.Liveweb.customObjects;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextView_F1 extends TextView {

    public TextView_F1(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextView_F1(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextView_F1(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                                               "fonts/jaapokki-regular.ttf");
        setTypeface(tf);
    }
}
