package com.SamerAlsayegh.Liveweb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.SamerAlsayegh.Liveweb.Enums.FieldValueType;
import com.SamerAlsayegh.Liveweb.Enums.ObjectType;
import com.SamerAlsayegh.Liveweb.Enums.TextStyle;
import com.SamerAlsayegh.Liveweb.Objects.WebObject;
import com.SamerAlsayegh.Liveweb.customObjects.Button_F1;
import com.SamerAlsayegh.Liveweb.customObjects.NavDrawerItem;
import com.SamerAlsayegh.Liveweb.utilities.Message;
import com.SamerAlsayegh.Liveweb.utilities.NavDrawerListAdapter;
import com.SamerAlsayegh.Liveweb.utilities.URLImageParser;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.TextView.BufferType;

@SuppressWarnings("deprecation")
public class EditorActivity extends FragmentActivity implements NumberPicker.OnValueChangeListener, View.OnTouchListener{
	public String chosenProjectName = null;
	public static EditorActivity instance;

	private static ArrayList<Integer> bolded = new ArrayList<Integer>();
	private static ArrayList<Integer> italic = new ArrayList<Integer>();
	private static HashMap<Integer, String> background = new HashMap<Integer, String>();

	private static HashMap<Integer, TextStyle> textStyle = new HashMap<Integer, TextStyle>();


	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;


	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	public static String loadedPage = null;

	public static Dialog dialogView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;

		setContentView(R.layout.activity_editor);

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		// Home
		for (int x = 0; x < navMenuTitles.length; x++){
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[x], navMenuIcons.getResourceId(0, -1)));
		}

		// Recycle the typed array
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, //nav menu toggle icon
				R.string.app_name, // nav drawer open - description for accessibility
				R.string.app_name // nav drawer close - description for accessibility
				) {
			public void onDrawerClosed(View view) {
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// We load a hint that there is a drawer of toolboxes.
		}
	}
	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
	ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			mDrawerLayout.closeDrawers();
			displayView(position);
		}
	}

	/* *
	 * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		// error in creating fragment
		switch (position){
		case 0:
			showDialog(ObjectType.Text);
			break;
		case 1:
			showDialog(ObjectType.Image);
			break;
		case 2:
			showDialog(ObjectType.Page);
			break;
		case 3:
			changePageDialog();
			break;
		default: 
			break;
		}


	}

	private void changePageDialog() {
		final Dialog dialog = new Dialog(instance, R.style.NewDialog);
		dialog.setContentView(R.layout.dialog_load_project);
		dialog.setCanceledOnTouchOutside(true);


		// Get ListView object from xml
		final ListView listView = (ListView) dialog.findViewById(R.id.projectsList);
		TextView textTitle = (TextView) dialog.findViewById(R.id.mainTitleLoad);
		textTitle.setText("Choose page to modify");

		Button_F1 button = (Button_F1) dialog.findViewById(R.id.cancelLoadProject);
		button.setText("Cancel");

		// Defined Array values to show in ListView
		String[] values = MainActivity.projects.get(MainActivity.openedProject).getPages().toArray(new String[MainActivity.projects.get(MainActivity.openedProject).getPages().size()]);

		// Define a new Adapter
		// First parameter - Context
		// Second parameter - Layout for the row
		// Third parameter - ID of the TextView to which the data is written
		// Forth - the Array of data

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.load_menu_list, R.id.Itemname, values);


		// Assign adapter to ListView
		listView.setAdapter(adapter); 

		// ListView Item Click Listener
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// ListView Clicked item value
				String itemValue = (String) listView.getItemAtPosition(position);
				loadedPage = itemValue;
				//reloadCurrentPage(MainActivity.openedProject);
				dialog.cancel();
			}
		}); 


		listView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				// ListView Clicked item value
				final String itemValue = (String) listView.getItemAtPosition(position);
				final Dialog dialog = new Dialog(instance, R.style.NewDialog);
				dialog.setContentView(R.layout.dialog_create_new_project);
				dialog.setCanceledOnTouchOutside(true);
				dialog.findViewById(R.id.projectName_input).setVisibility(View.GONE);
				dialog.findViewById(R.id.main_headline).setVisibility(View.GONE);
				Button_F1 cancel = (Button_F1) dialog.findViewById(R.id.cancelCreation);
				Button_F1 submit = (Button_F1) dialog.findViewById(R.id.submitCreation);
				cancel.setText("Cancel");
				submit.setText("Delete");
				submit.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						//deleteProject(itemValue);
						dialog.cancel();
					}
				});
				cancel.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View v) {
						dialog.cancel();
					}
				});
				dialog.show();
				return true;
			}
		}); 


		dialog.findViewById(R.id.cancelLoadProject).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				// Cancel the load menu.
				dialog.cancel();
			}
		});

		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@SuppressWarnings("deprecation")
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	public static ArrayList<Integer> loopedBold = new ArrayList<Integer>();
	public static ArrayList<Integer> loopedItalic = new ArrayList<Integer>();
	public static HashMap<TextStyle, String> textFormatting = new HashMap<TextStyle, String>();


	@SuppressLint("DefaultLocale") public static void showDialog(final ObjectType obType) {
		final Dialog dialog = new Dialog(instance, R.style.NewDialog);
		dialog.setContentView(R.layout.dialog_object_filler);
		dialog.setCanceledOnTouchOutside(true);
		LinearLayout layout = (LinearLayout) dialog.findViewById(R.id.inputFields);
		TextView title = (TextView) dialog.findViewById(R.id.mainTitle);
		title.setText(Message.format(instance.getString(R.string.dialogFillingTitle), obType.toString().toLowerCase(), "%objectType"));
		for (Entry<String, FieldValueType> entry : MainActivity.fieldPlaceHolders.get(obType).entrySet()){
			if (entry.getValue() == FieldValueType.String){
				final EditText textVal = new EditText(instance);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				if (entry.getKey().contains("*")){
					textVal.setOnFocusChangeListener (new OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus){
								if(textVal.getText().toString().isEmpty()){
									textVal.setError(Html.fromHtml("<font color='red'>A value must be entered!</font>"));
								}
								else
									textVal.setError(null);
							}
						}
					});
				}
				layout.addView(textVal, lp);
				textVal.setHint(entry.getKey().replace("*", ""));
			}
			else if (entry.getValue() == FieldValueType.HTML_String){
				final EditText textVal = new EditText(instance);
				textVal.setSingleLine(false);
				LinearLayout.LayoutParams lpText = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				textVal.setLayoutParams(lpText);

				textVal.addTextChangedListener(new TextWatcher(){


					public void afterTextChanged(Editable s) {

					}
					public void beforeTextChanged(CharSequence s, int start, int count, int after){


					}
					public void onTextChanged(CharSequence sequence, int start, int before, int count){
						// Update the new currentLoc based on where the selector is.
						currentLoc = textVal.getSelectionStart();

						Spannable str = textVal.getText();


						// Lines written by the user : Based on the use of 'next line' enter-button and not on the visual next line.

						String textWritten = (String) textVal.getLayout().getText().toString();
						Message.debug("Written so far: " + textVal.getText());
						String[] lines = (textWritten+"").split("\n");

						/*/
						for (int x = 0; x < bolded.size(); x++){
							if (textWritten.substring(bolded.get(x), bolded.get(x)+1).equalsIgnoreCase(" ")){
								// No need to edit the index.
							}
							else{

							}
						}
						 */

						if (bolded.size() > 0){
							if (bolded.size() % 2 == 0){
								for (int stylePosition = 0; stylePosition < bolded.size(); stylePosition += 2){
									if (!loopedBold.contains(stylePosition)){
										Message.debug("Looped at "  + stylePosition + bolded.get(stylePosition) + " and " + bolded.get(stylePosition + 1));
										str.setSpan(new StyleSpan(Typeface.BOLD), bolded.get(stylePosition), bolded.get(stylePosition + 1), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);								
										loopedBold.add(stylePosition);
									}
								}
							}

							if (bolded.size() % 2 == 1)
								str.setSpan(new StyleSpan(Typeface.BOLD), bolded.get(bolded.size() - 1), str.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						}



						if (italic.size() > 0){
							if (italic.size() % 2 == 0){
								for (int stylePosition = 0; stylePosition < italic.size(); stylePosition += 2){
									if (!loopedItalic.contains(stylePosition)){
										Message.debug("Looped at "  + stylePosition + italic.get(stylePosition) + " and " + italic.get(stylePosition + 1));
										str.setSpan(new StyleSpan(Typeface.ITALIC), italic.get(stylePosition), italic.get(stylePosition + 1), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);								
										loopedItalic.add(stylePosition);
									}
								}
							}
							if (italic.size() % 2 == 1)
								str.setSpan(new StyleSpan(Typeface.ITALIC), italic.get(italic.size() - 1), str.toString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);								
						}


					}
				});
				if (entry.getKey().contains("*")){
					textVal.setOnFocusChangeListener (new OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus){
								if(textVal.getText().toString().isEmpty()){
									textVal.setError(Html.fromHtml("<font color='red'>A link must be entered, in the form: </font><br><font color='gray'>http://www.example.com</font><br><center>or</center><br><font color='gray'>https://www.example.com</font>"));
								}
								else
									textVal.setError(null);
							}
						}
					});
				}

				layout.addView(textVal);
				textVal.setHint(entry.getKey().replace("*", ""));
			}
			else if (entry.getValue() == FieldValueType.HTML_Link){
				final EditText textVal = new EditText(instance);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				if (entry.getKey().contains("*")){
					textVal.setOnFocusChangeListener (new OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus){
								if(textVal.getText().toString().isEmpty()){
									textVal.setError(Html.fromHtml("<font color='red'>A link must be entered, in the form: </font><br><font color='gray'>http://www.example.com</font><br><center>or</center><br><font color='gray'>https://www.example.com</font>"));
								}
								else
									textVal.setError(null);
							}
						}
					});
				}
				layout.addView(textVal, lp);
				textVal.setHint(entry.getKey().replace("*", ""));
			}
			else if (entry.getValue() == FieldValueType.ID){
				final EditText textVal = new EditText(instance);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				textVal.setSingleLine(true);
				if (entry.getKey().contains("*")){
					textVal.setOnFocusChangeListener (new OnFocusChangeListener() {
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							if (!hasFocus){
								if(textVal.getText().toString().isEmpty()){
									textVal.setError(Html.fromHtml("<font color='red'>A unique value must be entered!</font>"));
								}
								else
									textVal.setError(null);
							}
						}
					});
				}
				layout.addView(textVal, lp);
				textVal.setHint(entry.getKey().replace("*", ""));
			}
			else if (entry.getValue() == FieldValueType.Number){
				NumberPicker textVal = new NumberPicker(instance);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
				textVal.setMaxValue(100);
				textVal.setMinValue(0);
				textVal.setWrapSelectorWheel(false);
				layout.addView(textVal, lp);
			}
		}

		Button_F1 button = (Button_F1) dialog.findViewById(R.id.cancelObject);
		button.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				dialogView.cancel();
			}
		});

		button = (Button_F1) dialog.findViewById(R.id.addObject);
		button.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View view) {
				// What to do to submit info.
				LinearLayout linearLayout = (LinearLayout) dialog.findViewById(R.id.inputFields);



				if (obType != ObjectType.Page){
					WebObject object = new WebObject(obType);

					int countRuns = 0;

					for (Entry<String, FieldValueType> objectInput : MainActivity.fieldPlaceHolders.get(obType).entrySet()){
						if (objectInput.getValue() == FieldValueType.String || objectInput.getValue() == FieldValueType.ID){
							if (linearLayout.getChildAt(countRuns) instanceof EditText){
								final EditText input = (EditText) linearLayout.getChildAt(countRuns);
								if (input.getHint().toString().equalsIgnoreCase("Text to Display")){
									if (!input.getText().toString().isEmpty())
										object.addDataSet("Text", input.getText().toString());
									else{
										object = null;
										break;
									}
								}
								else if (input.getHint().toString().equalsIgnoreCase("ID")){
									if (!input.getText().toString().isEmpty()){
										object.setID(input.getText().toString());
									}
									else{
										object = null;
										break;
									}
								}
								else if (input.getHint().toString().equalsIgnoreCase("Customizations"))
									object.formatStyle(input.getText().toString());
								else if (input.getHint().toString().equalsIgnoreCase("Pre-configured Styles"))
									object.setClassName(input.getText().toString());
								else if (input.getHint().toString().equalsIgnoreCase("Name"))
									object.setObjectName(input.getText().toString());
							}
						}
						countRuns++;
					}

					if (object != null){
						addObject(object);
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						RelativeLayout mainLayout = (RelativeLayout) instance.findViewById(R.id.frame_container);

						if (object.getObjectType().equals(ObjectType.Text)){
							TextView textObject = new TextView(instance);
							textObject.setText(Html.fromHtml("<p" + (object.getClassName() != null ? " class=\"" + object.getClassName() + "\"" : "") + ">" + object.getDataSet("Text") + "</p>"));
							textObject.setOnTouchListener(instance);

							mainLayout.addView(textObject, params);
						}
						else if (object.getObjectType().equals(ObjectType.Link)){
							TextView textObject = new TextView(instance);
							textObject.setText(Html.fromHtml("<a" + (object.getClassName() != null ? " class=\"" + object.getClassName() + "\"" : "") + " href=\"" + object.getDataSet("Link") + "\">" + object.getDataSet("Text") + "</a>"));
							textObject.setOnTouchListener(instance);
							mainLayout.addView(textObject, params);
						}
						else if (object.getObjectType().equals(ObjectType.Image)){
							TextView textObject = new TextView(instance);
							URLImageParser p = new URLImageParser(textObject, instance);
							Spanned htmlSpan = Html.fromHtml("<img" + (object.getClassName() != null ? " class=\"" + object.getClassName() + "\"" : "") + " src=\"" + object.getDataSet("Image") + "\"/>", p, null);
							textObject.setText(htmlSpan);
							textObject.setOnTouchListener(instance);
							mainLayout.addView(textObject, params);
						}

						/*
							WebView newObject = new WebView(instance);
							newObject.setOnTouchListener(instance);
							DisplayMetrics displaymetrics = new DisplayMetrics();
							instance.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
							int width = displaymetrics.widthPixels;

							RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width - (width/5), ViewGroup.LayoutParams.WRAP_CONTENT);
							RelativeLayout mainLayout = (RelativeLayout) instance.findViewById(R.id.frame_container);
							String summary = "<html style><table border=\"1\" style=\"width:100%\"><tr><td>Jill</td><td>Smith</td> <td>50</td></tr><tr><td>Eve</td><td>Jackson</td> <td>94</td></tr></table>";
							newObject.loadData(summary, "text/html", null);
							newObject.getSettings().setLoadWithOverviewMode(true);
							newObject.getSettings().setUseWideViewPort(true);
							mainLayout.addView(newObject, params);
						 */


						MainActivity.rebuildAllFiles(MainActivity.openedProject);
						dialogView.dismiss();
					}
				}
				else {
					int countRuns = 0;
					String pageName = null;
					for (Entry<String, FieldValueType> objectInput : MainActivity.fieldPlaceHolders.get(obType).entrySet()){
						if (objectInput.getValue() == FieldValueType.String || objectInput.getValue() == FieldValueType.ID){
							if (linearLayout.getChildAt(countRuns) instanceof EditText){
								final EditText input = (EditText) linearLayout.getChildAt(countRuns);
								if (input.getHint().toString().equalsIgnoreCase("Page Name")){
									if (!input.getText().toString().isEmpty())
										pageName = input.getText().toString();
									else{
										pageName = null;
										break;
									}
								}
							}
						}
						countRuns++;
					}

					if (pageName != null){
						MainActivity.projects.get(MainActivity.openedProject).addPage(pageName);
						MainActivity.updateXML(MainActivity.openedProject);
						MainActivity.rebuildAllFiles(MainActivity.openedProject);
						dialogView.dismiss();
					}
				}
			}
		});
		dialog.setCanceledOnTouchOutside(true);
		dialogView = dialog;
		dialog.show();
	}

	private static void addObject(WebObject object) {
		MainActivity.projects.get(MainActivity.openedProject).addClassesObject(loadedPage, object);
		MainActivity.rebuildAllFiles(MainActivity.openedProject);
		MainActivity.updateXML(MainActivity.openedProject);
	}

	@Override
	public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
		Log.i("value is",""+newVal);
	}
	/*
	public static void reloadCurrentPage(String openedProject) {
		// This method will be used to show a 'preview'
		File loadedDirectoryFile = new File(Environment.getExternalStorageDirectory() + "/WebEdit/Projects/" + openedProject + "/", "code");
		File loadedPageFile = new File(loadedDirectoryFile, loadedPage + ".html");
		WebView myWebView = (WebView) instance.findViewById(R.id.webView);
		myWebView.addJavascriptInterface(new WebAppInterface(instance), "Android");
		WebSettings webSettings = myWebView.getSettings();
		webSettings.setBuiltInZoomControls(false);
		webSettings.setJavaScriptEnabled(true);
		myWebView.loadUrl("file:///" + loadedPageFile.getAbsolutePath());
	}
	 */
	int _xDelta = 0;
	int _yDelta = 0;

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		final int X = (int) event.getRawX();
		final int Y = (int) event.getRawY();
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN:
			RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
			_xDelta = X - lParams.leftMargin;
			_yDelta = Y - lParams.topMargin;

			break;
		case MotionEvent.ACTION_UP:
			break;
		case MotionEvent.ACTION_POINTER_DOWN:
			break;
		case MotionEvent.ACTION_POINTER_UP:
			break;
		case MotionEvent.ACTION_MOVE:
			RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
			layoutParams.leftMargin = X - _xDelta;
			layoutParams.topMargin = Y - _yDelta;
			layoutParams.rightMargin = -250;
			layoutParams.bottomMargin = -250;
			view.setLayoutParams(layoutParams);
			break;
		}
		RelativeLayout mainLayout = (RelativeLayout) instance.findViewById(R.id.frame_container);
		mainLayout.invalidate();
		return true;
	}


	boolean boldEnabled = false;
	boolean italicEnabled = false;
	boolean underlineEnabled = false;
	boolean resizeEnabled = false;
	boolean colourEnabled = false;
	boolean backgroundEnabled = false;
	ImageButton btn = null;
	static Integer currentLoc = 0;
	public void boldText (View view){
		boldEnabled = !boldEnabled;
		btn = (ImageButton) view;
		btn.setActivated(boldEnabled);
		bolded.add(currentLoc);


		if (textStyle.containsKey(currentLoc)){
			if (textStyle.get(currentLoc).equals(TextStyle.ITALIC))
				textStyle.put(currentLoc, TextStyle.ITALIC_BOLD);
		}
		else
			textStyle.put(currentLoc, TextStyle.BOLD);
	}

	public void italicText (View view){
		italicEnabled = !italicEnabled;
		btn = (ImageButton) view;
		btn.setActivated(italicEnabled);
		italic.add(currentLoc);


		if (textStyle.containsKey(currentLoc)){
			if (textStyle.get(currentLoc).equals(TextStyle.BOLD))
				textStyle.put(currentLoc, TextStyle.ITALIC_BOLD);
		}
		else
			textStyle.put(currentLoc, TextStyle.ITALIC);
	}

	public void underlineText (View view){
		underlineEnabled = !underlineEnabled;
		btn = (ImageButton) view;
		btn.setActivated(underlineEnabled);
	}

	public void resizeText (View view){
		// Load resize dialog

	}

	public void colourText (View view){
		// Load colour wheel

	}

	public void backgroundText (View view){
		// Load colour wheel
	}

	public void openBasic (View view){
		Fragment_HTMLEditor.mViewPager.setCurrentItem(0);

	}
	public void openAdvanced (View view){
		Fragment_HTMLEditor.mViewPager.setCurrentItem(1);

	}

	public HashMap<TextStyle, String> parseHTMLSpecialLocationa(EditText textVal){
		String textWritten = (String) textVal.getLayout().getText().toString();
		int end = textWritten.length() - 1;
		int start = 0;

		int next;
		for (int i = start; i < end; i = next) {
			next = textVal.getText().nextSpanTransition(i, end, CharacterStyle.class);
			CharacterStyle[] style = textVal.getText().getSpans(i, next,
					CharacterStyle.class);

			for (int j = 0; j < (style.length > 1 ? 1 : style.length); j++) {
				if (style[j] instanceof StyleSpan) {
					int s = ((StyleSpan) style[j]).getStyle();
					
					if ((s - Typeface.BOLD) == 0){
						if (textFormatting.containsKey(TextStyle.BOLD)){
							if (textFormatting.get(TextStyle.BOLD).contains(";" + i + "," + (next-1))){
								Message.debug("Replacing prev value.");
								textFormatting.put(TextStyle.BOLD, textFormatting.get(TextStyle.BOLD).replace( i + "," + (next-1) + ";", i + "," + (next) + ";"));
							}
							else
								textFormatting.put(TextStyle.BOLD, textFormatting.get(TextStyle.BOLD) + i + "," + next + ";");
						}
						else
							textFormatting.put(TextStyle.BOLD, i + "," + next + ";");
						
						Message.debug("Character is bolded " + textVal.getText().toString().substring(i, next));
					}
					else if ((s - Typeface.ITALIC) == 0){
						textFormatting.put(TextStyle.ITALIC, (textFormatting.containsKey(TextStyle.ITALIC) ? textFormatting.get(TextStyle.ITALIC) + ";" + i + "," + next : i + "," + next));
						Message.debug("Character is italic " + textVal.getText().toString().substring(i, next));
					}
				}
			}
		}
		Message.debug("Re-iterating the prev form... " + textFormatting.get(TextStyle.BOLD));

		for (TextStyle var : textFormatting.keySet()){
			String varString = textFormatting.get(var);
			String[] splitString  = varString.split(";");
			ArrayList<String> listedValues = new ArrayList<String>();


			if (splitString.length >= 2){
				for (int x = 0; x < splitString.length - 1; x++){
					if (!listedValues.contains(splitString[x])){
						Message.debug("Looped through the entry " +  splitString[x]);
						listedValues.add(splitString[x]);
						if (splitString[x].split(",")[1].equalsIgnoreCase(splitString[x+1].split(",")[0])){
							Message.debug("[1] Matched - at " + splitString[x] + " and " + splitString[x+1]);
							varString = varString.replace(splitString[x] + ";" + splitString[x + 1], splitString[x].split(",")[0] + "," + splitString[x+1].split(",")[1]);
						}
						if (splitString[x].split(",")[0].equalsIgnoreCase(splitString[x+1].split(",")[0])){
							Message.debug("[2] Matched  at " + splitString[x] + " and " + splitString[x+1]);
							varString = varString.replace(splitString[x] + ";" + splitString[x + 1], splitString[x].split(",")[0] + "," + splitString[x+1].split(",")[1]);
						}
						textFormatting.put(var, varString);
					}
				}
			}
			Message.debug("Re-iterating the true form... " + textFormatting.get(var));
			if (varString != null){
				String[] formattedSplitString = varString.split(";");
				for (String splitString2 : formattedSplitString){
					Message.debug(var.name() + " | " + textWritten.substring(Integer.parseInt(splitString2.split(",")[0]), Integer.parseInt(splitString2.split(",")[1])));
				}
			}
		}
		return textFormatting;
	}
}
