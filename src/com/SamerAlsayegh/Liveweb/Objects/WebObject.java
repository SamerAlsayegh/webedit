package com.SamerAlsayegh.Liveweb.Objects;

import java.util.LinkedHashMap;

import com.SamerAlsayegh.Liveweb.MainActivity;
import com.SamerAlsayegh.Liveweb.Enums.ObjectType;
import com.SamerAlsayegh.Liveweb.utilities.WebTools;

public class WebObject {
	String ID = null;
	ObjectType obType = ObjectType.Text;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> styles = null;
	String className = null;
	String objectName = null;
	String projectName = null;
	
	public WebObject (ObjectType type){
		obType = type;
		styles = new LinkedHashMap<String, String>();
	}

	/**
	 * Set the ID of the object
	 * @return Void
	 */
	public void setID(String ID_){
		ID = WebTools.validID(ID_);
	}
	
	/**
	 * Sets object type... So we can use correct code addition..
	 * @return Void
	 */
	public void setObjectType(ObjectType type){
		obType = type;
	}
	
	/**
	 * Gets the object type for correct code syntax.
	 * @return ObjectType Value.
	 */
	public ObjectType getObjectType(){
		return obType;
	}
	/**
	 * Adds style to the object.
	 * @return Void
	 */
	public void addStyle(String style, String styleValue){
		styles.put(style, styleValue);
	}

	/**
	 * Remove style of the object.
	 * @return Void
	 */
	public void removeStyle(String style){
		styles.remove(style);
	}

	/**
	 * Set the class used for pre-defined style
	 * @return Void
	 */
	public void setClassName(String className_){
		className = className_;
	}

	/**
	 * Set object's name.
	 * @return Void
	 */
	public void setObjectName(String objectName_){
		objectName = objectName_;
	}
	
	/**
	 * Set the project that this object is under.
	 * @return Void
	 */
	public void setProject(String projectName_){
		projectName = projectName_;
	}
	
	/**
	 * Set the ID of the object
	 * @return 
	 * @return Void
	 */
	public String getID(){
		return ID;
	}
	
	/**
	 * Get the ClassObject name that belong's to the object.
	 * @return ClassObject name that belongs to this object.
	 */
	public String getClassName(){
		return className;
	}
	
	/**
	 * Get the ClassObject that belong's to the object.
	 * @return ClassObject that belongs to this text field.
	 */
	public classObject getClassObject(){
		return MainActivity.projects.get(MainActivity.openedProject).getClassesObject(className);
	}
	
	/**
	 * Add custom data such as;
	 * Width;
	 * Height;
	 * Custom Tags for object;
	 * @return Void
	 */
	public void addDataSet(String key, String value){
		dataSet.put(key, value);
	}
	
	/**
	 * Remove custom data such as;
	 * Width;
	 * Height;
	 * Custom Tags for object;
	 * @return Void
	 */
	public void removeDataSet(String key){
		dataSet.remove(key);
	}
	
	/**
	 * Get the dataset value, based on the name.
	 * @return Data set value
	 */
	public String getDataSet(String key){
		return dataSet.get(key);
	}
	
	/**
	 * Get the dataset list.
	 * @return Dataset list
	 */
	public LinkedHashMap<String, String> getFullDataSet(){
		return dataSet;
	}
	
	public void formatStyle(String string) {
		// TODO Auto-generated method stub
		
	}

}
