package com.SamerAlsayegh.Liveweb.Objects;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class WebsiteData {
	
	public HashMap<String, String> styles = null;

	public WebsiteData (){
		styles = new HashMap<String, String>();
	}
	
	/**
	 * Adds style to the object.
	 *
	 * @return Void
	 */
	public void addStyle(String style, String styleValue){
		styles.put(style, styleValue);
	}
	
	/**
	 * Remove style of the object.
	 *
	 * @return Void
	 */
	public void removeStyle(String style){
		styles.remove(style);
	}
	
	/**
	 * Get all styles for this class
	 *
	 * @return Get styles and values of this class
	 */
	public Set<Entry<String, String>> getStyles(){
		return styles.entrySet();
	}
	
	/**
	 * Get style's value based on the style name.
	 *
	 * @return Get style value, based off of style name.
	 */
	public String getStyle(String style){
		return styles.get(style);
	}
}
