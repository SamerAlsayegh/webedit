package com.SamerAlsayegh.Liveweb.Enums;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

import com.SamerAlsayegh.Liveweb.Objects.WebObject;
import com.SamerAlsayegh.Liveweb.Objects.classObject;

public class ProjectInfo {

	Integer projectID;
	String projectName;
	String projectPath;
	Integer projectCreation;
	Integer projectModified;
	String websiteName;
	LinkedHashMap<String, classObject> classesList = new LinkedHashMap<String, classObject>();
	LinkedHashMap<String, LinkedList<WebObject>> objectsPage = new LinkedHashMap<String, LinkedList<WebObject>>();


	public ProjectInfo(String projectName){
		this.projectName = projectName;
	}

	public void setProjectID(Integer projectID){
		this.projectID = projectID;
	}

	public void setProjectName(String projectName){
		this.projectName = projectName;
	}

	public void setProjectPath(String projectPath){
		this.projectPath = projectPath;
	}

	public void setProjectCreation(Integer projectCreation){
		this.projectCreation = projectCreation;
	}

	public void setProjectModified(Integer projectModified){
		this.projectModified = projectModified;
	}

	public void setWebsiteName(String websiteName){
		this.websiteName = websiteName;
	}

	public void addClass(String className, classObject classObject){
		classesList.put(className, classObject);
	}

	public void addClassesObject(String pageName, WebObject object){
		if (!objectsPage.containsKey(pageName))
			objectsPage.put(pageName, new LinkedList<WebObject>());
		objectsPage.get(pageName).add(object);
	}
	
	public void addPage(String pageName){
		if (!objectsPage.containsKey(pageName))
			objectsPage.put(pageName, new LinkedList<WebObject>());
	}
	
	public void removeClass(String className, classObject classObject){
		classesList.remove(className);
	}

	public void removeClassesObject(String pageName, WebObject object){
		if (!objectsPage.containsKey(pageName))
			objectsPage.put(pageName, new LinkedList<WebObject>());
		objectsPage.get(pageName).remove(object);
	}
	
	public Integer getProjectID(){
		return projectID;
	}

	public String getProjectName(){
		return projectName;
	}

	public String getProjectPath(){
		return projectPath;
	}

	public Integer getProjectCreation(){
		return projectCreation;
	}

	public Integer getProjectModified(){
		return projectModified;
	}

	public String getWebsiteName(){
		return websiteName;
	}

	public LinkedHashMap<String, classObject> getClasses(){
		return classesList;
	}

	public classObject getClassesObject(String className){
		return classesList.get(className);
	}

	public LinkedList<WebObject> getObjectsList(String pageName){
		return (objectsPage.containsKey(pageName) ? objectsPage.get(pageName) : new LinkedList<WebObject>());
	}
	
	public Set<String> getPages(){
		return objectsPage.keySet();
	}
	
	public Set<Entry<String, LinkedList<WebObject>>> getObjects(){
		return objectsPage.entrySet();
	}
}
