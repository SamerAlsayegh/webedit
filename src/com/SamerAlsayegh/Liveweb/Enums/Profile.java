package com.SamerAlsayegh.Liveweb.Enums;

import java.util.Date;

public class Profile {
	ProfileName name;
	long birthDate;
	Gender gender = Gender.Undefined;
	String email;
	String profilePictureLink;
	
	public Profile (String firstName, String lastName){
		name = new ProfileName(firstName, lastName);
	}
	
	public ProfileName getName(){
		return name;
	}
	public Date getBirthdate(){
		return new Date(birthDate * 1000);
	}
	public Integer getCurrentAge(){
		return (int) (((System.currentTimeMillis()/1000) - birthDate) / 31536000);
	}
	public Gender getGender(){
		return gender;
	}
	public String getEmail(){
		return email;
	}
	
	public void setName(ProfileName name){
		this.name = name;
	}
	
	public void setBirthdate(long l){
		birthDate = l;
	}
	
	public void setGender(Gender gender){
		this.gender = gender;
	}
	public void setEmail(String email){
		this.email = email;
	}
}
