package com.SamerAlsayegh.Liveweb.Enums;

public enum ObjectType {
	Text, Image, Video, Style, Custom, Class, Link, HorizontalLine, Menu, Page
}
