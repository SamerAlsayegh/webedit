package com.SamerAlsayegh.Liveweb.Enums;

public class ProfileName {
	String firstName;
	String lastName;
	String middleName;
	String nameSuffix;
	String namePrefix;
	String nameTitle;
	String displayName;
	
	public ProfileName (String firstName, String lastName){
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	
	public String getFirstName(){
		return firstName;
	}
	public String getLastName(){
		return lastName;
	}
}
