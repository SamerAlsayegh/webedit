package com.SamerAlsayegh.Liveweb.Enums;

public enum TextStyle {
	BOLD, ITALIC, ITALIC_BOLD, UNDERLINE
}
