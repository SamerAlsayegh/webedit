package com.SamerAlsayegh.Liveweb.Enums;

public enum FieldValueType {
	String, Number, StringEntry, Dimension, Dropdown, Link, StyleBuilder, Image, ID, HTML_String, HTML_Link
}
