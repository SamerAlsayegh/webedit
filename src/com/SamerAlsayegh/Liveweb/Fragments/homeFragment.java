package com.SamerAlsayegh.Liveweb.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.SamerAlsayegh.Liveweb.MainActivity;
import com.SamerAlsayegh.Liveweb.R;

public class homeFragment extends Fragment {
	static View rootView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_section_home, container, false);
		
		MainActivity.homeFragmentView = rootView;
		/*
		 * Font set-up
		 */
		Button btn;

		if (MainActivity.projects.size() > 0){
			btn = (Button) rootView.findViewById(R.id.continueLastProject);
			if (MainActivity.openedProject != null)
				btn.setVisibility(View.VISIBLE);
			btn = (Button) rootView.findViewById(R.id.newProject);

			btn = (Button) rootView.findViewById(R.id.loadProject);
			if (MainActivity.projects.size() > 1)
				btn.setVisibility(View.VISIBLE);
		}

		return rootView;
	}

	public static void setupFBLogin() {
		TextView hello = (TextView) rootView.findViewById(R.id.helloMate);
		hello.setVisibility(View.GONE);
	}
}
