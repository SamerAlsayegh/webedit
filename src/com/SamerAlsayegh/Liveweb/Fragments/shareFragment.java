package com.SamerAlsayegh.Liveweb.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.SamerAlsayegh.Liveweb.R;
import com.SamerAlsayegh.Liveweb.sharing.Facebook;

public class shareFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_section_share, container, false);
		
		return rootView;
	}
}