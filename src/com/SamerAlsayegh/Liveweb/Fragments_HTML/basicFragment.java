package com.SamerAlsayegh.Liveweb.Fragments_HTML;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.SamerAlsayegh.Liveweb.MainActivity;
import com.SamerAlsayegh.Liveweb.R;

public class basicFragment extends Fragment {
	static View rootView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_section_basic, container, false);
		
		return rootView;
	}
}
