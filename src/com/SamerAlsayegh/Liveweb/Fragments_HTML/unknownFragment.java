package com.SamerAlsayegh.Liveweb.Fragments_HTML;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.SamerAlsayegh.Liveweb.R;

/**
 * A fragment that is just empty and is an error page.
 */
public class unknownFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_section_404, container, false);
		return rootView;
	}
}