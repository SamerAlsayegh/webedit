package com.SamerAlsayegh.Liveweb.Fragments_HTML;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.SamerAlsayegh.Liveweb.R;
import com.SamerAlsayegh.Liveweb.sharing.Facebook;

public class advancedFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_section_advanced, container, false);
		
		return rootView;
	}
}